import React, { Component } from 'react'

import { observer, Provider } from 'mobx-react'

import AppNavigator from './AppNavigator'
import UserStore from './src/store/UserStore'

@observer
class App extends Component {
  render() {
    return (
      <Provider userStore={UserStore}>
        <AppNavigator />
      </Provider>
    )
  }
}

export default App
