/* @flow weak */

import React from 'react'
import { View, Text, StyleSheet, TextInput } from 'react-native'

import constants from '../config/constants'

const CustomTextInput = ({ label, ...otherProps }) => (
  <View style={styles.container}>
    <Text style={styles.label}>{label}</Text>
    <View style={styles.textInputContainer}>
      <TextInput
        style={{ flex: 1 }}
        underlineColorAndroid={'transparent'}
        {...otherProps}
      />
    </View>
  </View>
)

export default CustomTextInput

const styles = StyleSheet.create({
  container: {
    height: 80,
    width: constants.DEVICE_WIDTH * 0.9,
    marginVertical: 5
  },

  label: {
    marginVertical: 5,
    color: 'black'
  },

  textInputContainer: {
    backgroundColor: 'white',
    borderRadius: 5,
    borderColor: constants.GREY,
    borderWidth: 0.3,
    flex: 1,
    paddingHorizontal: 10,
    paddingVertical: 0,
    flexDirection: 'row'
  }
})
