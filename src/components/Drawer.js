/* @flow weak */

import React from 'react'
import { View, Text, StyleSheet, ScrollView, Image } from 'react-native'
import { SafeAreaView, DrawerItems } from 'react-navigation'

import constants from '../config/constants'

const PICTURE = require('../../assets/image_employee_placeholder.png')

const Drawer = props => (
  <ScrollView>
    <SafeAreaView
      style={styles.container}
      forceInset={{ top: 'always', horizontal: 'never' }}
    >
      <View style={styles.header}>
        <Image source={PICTURE} />
        <Text style={styles.caption}>e-Pelayanan{'\n'}Kecamatan Rappocini</Text>
      </View>
      <DrawerItems {...props} />
    </SafeAreaView>
  </ScrollView>
)

export default Drawer

const styles = StyleSheet.create({
  container: {
    flex: 1
  },

  header: {
    backgroundColor: constants.PRIMARY_COLOR,
    padding: 10,
    flexDirection: 'row',
    alignItems: 'center'
  },

  caption: {
    color: 'white',
    fontWeight: 'bold'
  }
})
