/* @flow weak */

import React from 'react'
import { View, Text, StyleSheet, Image, TouchableOpacity } from 'react-native'

import constants from '../config/constants'

const PLACEHOLDER = require('../../assets/image_employee_placeholder.png')

const EmployeeItem = ({
  name,
  address,
  phone,
  rt,
  position,
  ...otherProps
}) => (
  <TouchableOpacity style={styles.container} {...otherProps}>
    <Text style={styles.label}>{position}</Text>
    <View style={styles.detailContainer}>
      <Image source={PLACEHOLDER} />
      <View style={{ marginLeft: 25 }}>
        <Text style={styles.detail}>Sugiarto S.T., M.T.</Text>
        <Text style={styles.detail}>Jl. Kenangan Indah No. 1</Text>
        <Text style={styles.detail}>+628133757475</Text>
        <Text style={styles.detail}>RT 5 RW 10</Text>
      </View>
    </View>
  </TouchableOpacity>
)

export default EmployeeItem

const styles = StyleSheet.create({
  container: {
    width: constants.DEVICE_WIDTH * 0.9,
    elevation: 5,
    padding: 15,
    marginVertical: 10,
    backgroundColor: 'white'
  },

  label: {
    color: 'black',
    fontSize: 15
  },

  detailContainer: {
    flexDirection: 'row',
    marginTop: 10
  },

  detail: {
    color: 'black'
  }
})
