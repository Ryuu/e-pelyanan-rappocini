/* @flow weak */

import React from 'react'
import { View, Text, StyleSheet, TouchableOpacity } from 'react-native'

import constants from '../config/constants'

const CustomButton = ({ label, ...otherProps }) => (
  <TouchableOpacity style={styles.container} {...otherProps}>
    <Text style={styles.label}>{label}</Text>
  </TouchableOpacity>
)

export default CustomButton

const styles = StyleSheet.create({
  container: {
    height: 40,
    elevation: 1,
    width: constants.DEVICE_WIDTH * 0.9,
    backgroundColor: constants.PRIMARY_COLOR,
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: 5,
    marginBottom: 30,
    borderRadius: 5
  },

  label: {
    color: 'white'
  }
})
