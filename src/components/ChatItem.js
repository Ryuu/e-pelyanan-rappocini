/* @flow weak */

import React from 'react'
import { View, Text, StyleSheet } from 'react-native'

import constants from '../config/constants'

const ChatItem = () => (
  <View style={styles.container}>
    <Text style={styles.content}>I'm ChatItem</Text>
  </View>
)

export default ChatItem

const styles = StyleSheet.create({
  container: {
    backgroundColor: constants.PRIMARY_COLOR,
    paddingHorizontal: 20,
    paddingVertical: 10,
    borderRadius: 5,
    alignSelf: 'flex-start',
    maxWidth: 200,
    marginVertical: 10
  },

  content: {
    color: 'white'
  }
})
