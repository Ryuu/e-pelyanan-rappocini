/* @flow weak */

import React from 'react'
import { View, Text, StyleSheet, TouchableOpacity } from 'react-native'

import constants from '../config/constants'

const RegionListItem = ({ name, ...otherProps }) => (
  <TouchableOpacity style={styles.container} {...otherProps}>
    <Text style={styles.label}>{name}</Text>
  </TouchableOpacity>
)

export default RegionListItem

const styles = StyleSheet.create({
  container: {
    height: 50,
    padding: 10,
    elevation: 5,
    width: constants.DEVICE_WIDTH * 0.9,
    backgroundColor: 'white',
    borderRadius: 5,
    justifyContent: 'center',
    marginVertical: 10,
    alignSelf: 'center'
  },

  label: {
    color: 'black'
  }
})
