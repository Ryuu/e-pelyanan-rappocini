/* @flow */

import React, { Component } from 'react'
import {
  View,
  Text,
  StyleSheet,
  StatusBar,
  Image,
  TextInput,
  FlatList
} from 'react-native'

import constants from '../config/constants'
import ChatItem from '../components/ChatItem'

const ICON_PHONE = require('../../assets/icon_phone.png')
const ICON_SEND = require('../../assets/icon_send.png')

export default class Contact extends Component {
  static navigationOptions = {
    title: 'Hubungi Kami',
    drawerIcon: () => <Image source={ICON_PHONE} />
  }

  render() {
    return (
      <View style={styles.container}>
        <StatusBar
          backgroundColor={constants.PRIMARY_COLOR}
          barStyle={'light-content'}
        />
        <FlatList
          data={CHATS}
          renderItem={({ item }) => <ChatItem />}
          inverted
          style={{ marginBottom: 70 }}
        />
        <View style={styles.textBar}>
          <TextInput
            placeholder={'Ketikkan pesan disini'}
            underlineColorAndroid={'transparent'}
            style={{ flex: 3 }}
          />
          <View
            style={{
              flex: 1,
              alignItems: 'flex-end',
              justifyContent: 'center',
              paddingRight: 10
            }}
          >
            <Image
              source={ICON_SEND}
              style={{ flex: 1, resizeMode: 'contain' }}
            />
          </View>
        </View>
      </View>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingHorizontal: 10
  },

  textBar: {
    position: 'absolute',
    bottom: 0,
    height: 60,
    flexDirection: 'row',
    width: constants.DEVICE_WIDTH,
    borderTopWidth: 1,
    borderColor: constants.GREY,
    padding: 10
  }
})

const CHATS = [{ id: 0 }, { id: 1 }, { id: 2 }]
