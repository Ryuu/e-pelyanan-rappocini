/* @flow */

import React, { Component } from 'react'
import { View, Text, StyleSheet, ScrollView } from 'react-native'

import constants from '../config/constants'
import EmployeeItem from '../components/EmployeeItem'

export default class RegionDetail extends Component {
  static navigationOptions = ({ navigation }) => ({
    title: navigation.getParam('name', 'Kelurahan')
  })

  render() {
    return (
      <ScrollView
        style={styles.container}
        contentContainerStyle={{ alignItems: 'center' }}
      >
        <Label label={'RW 01'} />
        <EmployeeItem position={'Ketua RW'} />
        <EmployeeItem position={'Ketua RT 01'} />
        <EmployeeItem position={'Ketua RT 02'} />
        <Label label={'RW 02'} />
        <EmployeeItem position={'Ketua RW'} />
        <EmployeeItem position={'Ketua RT 01'} />
        <EmployeeItem position={'Ketua RT 02'} />
      </ScrollView>
    )
  }
}

const Label = ({ label }) => (
  <View style={styles.labelContainer}>
    <Text style={styles.label}>{label}</Text>
  </View>
)

const styles = StyleSheet.create({
  container: {
    flex: 1
  },

  labelContainer: {
    paddingHorizontal: 30,
    paddingVertical: 10,
    borderBottomWidth: 1,
    borderColor: constants.GREY
  },

  label: {
    color: 'black'
  }
})
