/* @flow */

import React, { Component } from 'react'
import { View, Text, StyleSheet, Image, StatusBar, WebView } from 'react-native'

import constants from '../config/constants'

const ICON_PROFILE = require('../../assets/icon_profile.png')

export default class Home extends Component {

  static navigationOptions = {
    title: 'Profil Kecamatan',
    drawerIcon: ({ tintColor }) => (
      <Image
        source={ICON_PROFILE}
      />
    )
  }

  render() {
    return (
      <View style={styles.container}>
        <StatusBar
          backgroundColor={constants.PRIMARY_COLOR}
          barStyle={'light-content'}
        />
        <WebView
          source={{ uri: 'http://www.kec-rappocini.id' }}
          style={{ flex: 1 }}
        />
      </View>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1
  }
})
