/* @flow */

import React, { Component } from 'react'
import { View, Text, StyleSheet, StatusBar } from 'react-native'

import EmployeeItem from '../components/EmployeeItem'

import constants from '../config/constants'

export default class EmployeeList extends Component {
  static navigationOptions = ({ navigation }) => ({
    title: navigation.getParam('name', null)
  })

  render() {
    return (
      <View style={styles.container}>
        <StatusBar
          backgroundColor={constants.PRIMARY_COLOR}
          barStyle={'light-content'}
        />
        <EmployeeItem position={'Petugas Kebersihan'} />
        <EmployeeItem position={'Petugas Kebersihan'} />
        <EmployeeItem position={'Petugas Kebersihan'} />
      </View>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1
  }
})
