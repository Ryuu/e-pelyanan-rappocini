/* @flow */

import React, { Component } from 'react'
import {
  View,
  Text,
  StyleSheet,
  Image,
  StatusBar,
  TouchableWithoutFeedback
} from 'react-native'

import constants from '../config/constants'

const ICON_ORGANIZATION = require('../../assets/icon_organization.png')
const ORGANIZATION_STRUCTURE = require('../../assets/organization_structure.png')

export default class Organization extends Component {
  static navigationOptions = {
    title: 'Struktur Organisasi',
    drawerIcon: () => <Image source={ICON_ORGANIZATION} />
  }

  render() {
    return (
      <View style={styles.container}>
        <StatusBar
          backgroundColor={constants.PRIMARY_COLOR}
          barStyle={'light-content'}
        />
        <TouchableWithoutFeedback
          onPress={() => this.props.navigation.navigate('RegionProfile')}
        >
          <Image source={ORGANIZATION_STRUCTURE} style={{ flex: 1 }} />
        </TouchableWithoutFeedback>
      </View>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white',
    justifyContent: 'center',
    alignItems: 'center'
  }
})
