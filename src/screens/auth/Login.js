/* @flow */

import React, { Component } from 'react'
import {
  View,
  Text,
  StyleSheet,
  Image,
  StatusBar,
  DeviceEventEmitter
} from 'react-native'
import { inject } from 'mobx-react'

import constants from '../../config/constants'

import CustomTextInput from '../../components/CustomTextInput'
import CustomButton from '../../components/CustomButton'

const LOGO = require('../../../assets/image_logo.png')

@inject('userStore')
export default class Login extends Component {
  static navigationOptions = {
    title: 'Login'
  }

  constructor(props) {
    super(props)
    this.state = {
      email: ''
    }
  }

  login() {
    if (this.state.email == 'admin') {
      this.props.navigation.navigate('Home')
      return
    }
    this.props.userStore.user = true
    this.props.navigation.goBack(null)
    DeviceEventEmitter.emit('should component update')
  }

  render() {
    return (
      <View style={styles.container}>
        <StatusBar
          backgroundColor={constants.PRIMARY_COLOR}
          barStyle={'light-content'}
        />
        <View style={styles.logoContainer}>
          <Image source={LOGO} />
          <Text style={styles.title}>e-Pelayanan</Text>
          <Text style={styles.title}>Kecamatan Rappocini</Text>
        </View>
        <View style={styles.formContainer}>
          <CustomTextInput
            label={'Email'}
            onChangeText={value => this.setState({ email: value })}
          />
          <CustomTextInput label={'Password'} />
          <CustomButton label={'Login'} onPress={() => this.login()} />
        </View>
      </View>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white'
  },

  logoContainer: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center'
  },

  title: {
    color: 'black',
    fontWeight: 'bold'
  },

  formContainer: {
    flex: 2,
    alignItems: 'center'
  }
})
