/* @flow */

import React, { Component } from 'react'
import { View, Text, StyleSheet, StatusBar } from 'react-native'

import constants from '../config/constants'

export default class Confirmation extends Component {
  static navigationOptions = {
    title: 'Konfirmasi'
  }

  render() {
    return (
      <View style={styles.container}>
        <StatusBar
          backgroundColor={constants.PRIMARY_COLOR}
          barStyle={'light-content'}
        />
        <Text>Data-data anda telah diterima</Text>
        <Text>Karyawan kami akan mengantarkan berkas ke rumah anda</Text>
      </View>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center'
  }
})
