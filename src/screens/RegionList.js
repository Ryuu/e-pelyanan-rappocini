/* @flow */

import React, { Component } from 'react'
import {
  View,
  Text,
  StyleSheet,
  StatusBar,
  Image,
  FlatList
} from 'react-native'

import constants from '../config/constants'
import RegionListItem from '../components/RegionListItem'

const ICON_LIST = require('../../assets/icon_list.png')

export default class RegionList extends Component {
  static navigationOptions = {
    title: 'Daftar RT / RW',
    drawerIcon: () => <Image source={ICON_LIST} />
  }

  render() {
    return (
      <View style={styles.container}>
        <StatusBar
          backgroundColor={constants.PRIMARY_COLOR}
          barStyle={'light-content'}
        />
        <FlatList
          data={REGION_LIST}
          renderItem={({ item }) => (
            <RegionListItem
              name={item.name}
              onPress={() =>
                this.props.navigation.navigate('RegionDetail', {
                  name: item.name
                })
              }
            />
          )}
          showsVerticalScrollIndicator={false}
        />
      </View>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    padding: 10
  }
})

const REGION_LIST = [
  {
    id: 0,
    name: 'Kelurahan Buakana'
  },
  {
    id: 1,
    name: 'Kelurahan Banta-Bantaeng'
  },
  {
    id: 2,
    name: 'Kelurahan Balla Parang'
  },
  {
    id: 3,
    name: 'Kelurahan Bonto Makkio'
  },
  {
    id: 4,
    name: 'Kelurahan Gunung Sari'
  },
  {
    id: 5,
    name: 'Kelurahan Karunrung'
  },
  {
    id: 6,
    name: 'Kelurahan Kassi-kassi'
  },
  {
    id: 7,
    name: 'Kelurahan Mappala'
  },
  {
    id: 8,
    name: 'Kelurahan Rappocini'
  },
  {
    id: 9,
    name: 'Kelurahan Tidung'
  },
  {
    id: 10,
    name: 'Kelurahan Minasa Upa'
  }
]
