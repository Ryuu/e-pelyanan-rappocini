/* @flow */

import React, { Component } from 'react'
import { View, Text, StyleSheet, StatusBar, Image } from 'react-native'

import constants from '../config/constants'

const IMAGE_PLACHOLDER = require('../../assets/image_employee_placeholder.png')
const ICON_PHONE = require('../../assets/icon_phone_circle.png')
const ICON_LOCATION = require('../../assets/icon_location_circle.png')

export default class RegionProfile extends Component {
  static navigationOptions = {
    title: 'Keluarahan Buakana'
  }

  render() {
    return (
      <View style={styles.container}>
        <StatusBar
          backgroundColor={constants.PRIMARY_COLOR}
          barStyle={'light-content'}
        />
        <View style={styles.imageContainer}>
          <Image source={IMAGE_PLACHOLDER} />
          <Text style={{ color: 'black', fontSize: 20, marginTop: 10 }}>
            Tampu Bolon
          </Text>
          <Text style={{ color: 'black' }}>Lurah Buakana</Text>
        </View>
        <View style={styles.detailContainer}>
          <View style={styles.detailItem}>
            <Image source={ICON_PHONE} />
            <View style={{ marginLeft: 10 }}>
              <Text style={{ color: 'black', fontSize: 10 }}>Telepon</Text>
              <Text style={{ color: 'black', fontSize: 16 }}>
                +628164537743
              </Text>
            </View>
          </View>
          <View style={styles.detailItem}>
            <Image source={ICON_LOCATION} />
            <View style={{ marginLeft: 10 }}>
              <Text style={{ color: 'black', fontSize: 10 }}>Alamat</Text>
              <Text style={{ color: 'black', fontSize: 16 }}>
                Jl. Kenangan Indah No. 1
              </Text>
            </View>
          </View>
        </View>
      </View>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white'
  },

  imageContainer: {
    alignItems: 'center',
    justifyContent: 'center',
    flex: 1,
    borderBottomWidth: 1,
    borderColor: constants.GREY
  },

  detailContainer: {
    padding: 10,
    flex: 2
  },

  detailItem: {
    flexDirection: 'row',
    padding: 10,
    marginVertical: 5
  }
})
