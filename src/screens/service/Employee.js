/* @flow */

import React, { Component } from 'react'
import { View, Text, StyleSheet, FlatList } from 'react-native'

import EmployeeItem from '../../components/EmployeeItem'
import RegionListItem from '../../components/RegionListItem'

import constants from '../../config/constants'

const now = new Date().getHours()

export default class Employee extends Component {
  static navigationOptions = {
    title: 'Karyawan'
  }

  renderEmployee() {
    return (
      <View style={[styles.container, { alignItems: 'center' }]}>
        <EmployeeItem position={'Petugas Motor Lorong'} />
        <EmployeeItem position={'Petugas Motor Lorong'} />
        <EmployeeItem position={'Petugas Motor Lorong'} />
      </View>
    )
  }

  renderRegions() {
    return (
      <View style={styles.container}>
        <FlatList
          data={REGION_LIST}
          renderItem={({ item }) => (
            <RegionListItem
              name={item.name}
              onPress={() =>
                this.props.navigation.navigate('EmployeeList', {
                  name: item.name
                })
              }
            />
          )}
          showsVerticalScrollIndicator={false}
        />
      </View>
    )
  }

  render() {
    if (now > 17) {
      return this.renderRegions()
    }
    return this.renderEmployee()
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingVertical: 10
  }
})

const REGION_LIST = [
  {
    id: 0,
    name: 'Kelurahan Buakana'
  },
  {
    id: 1,
    name: 'Kelurahan Banta-Bantaeng'
  },
  {
    id: 2,
    name: 'Kelurahan Balla Parang'
  },
  {
    id: 3,
    name: 'Kelurahan Bonto Makkio'
  },
  {
    id: 4,
    name: 'Kelurahan Gunung Sari'
  },
  {
    id: 5,
    name: 'Kelurahan Karunrung'
  },
  {
    id: 6,
    name: 'Kelurahan Kassi-kassi'
  },
  {
    id: 7,
    name: 'Kelurahan Mappala'
  },
  {
    id: 8,
    name: 'Kelurahan Rappocini'
  },
  {
    id: 9,
    name: 'Kelurahan Tidung'
  },
  {
    id: 10,
    name: 'Kelurahan Minasa Upa'
  }
]
