/* @flow */

import React, { Component } from 'react'
import {
  View,
  Text,
  StyleSheet,
  ScrollView,
  Picker,
  DeviceEventEmitter
} from 'react-native'
import { inject, observer } from 'mobx-react'

import constants from '../../config/constants'

import CustomTextInput from '../../components/CustomTextInput'
import CustomButton from '../../components/CustomButton'

@inject('userStore')
export default class Form extends Component {
  constructor(props) {
    super(props)
    this.state = {
      letterType: 'pbb',
      loginState: this.props.userStore.user
    }
  }

  componentDidMount() {
    DeviceEventEmitter.addListener('should component update', () => {
      this.setState({ loginState: this.props.userStore.user })
    })
  }

  renderContent() {
    return (
      <ScrollView
        style={styles.container}
        contentContainerStyle={{ alignItems: 'center' }}
      >
        <CustomTextInput
          label={'Nama (sesuai KTP)'}
          placeholder={'Nama'}
          returnKeyType={'next'}
          autoCapitalize={'words'}
        />
        <CustomTextInput
          label={'NIK'}
          placeholder={'NIK'}
          returnKeyType={'next'}
          keyboardType={'numeric'}
        />
        <CustomTextInput
          label={'Alamat'}
          placeholder={'Alamat'}
          returnKeyType={'next'}
          autoCapitalize={'none'}
        />
        <CustomTextInput
          label={'Kelurahan'}
          placeholder={'Kelurahan'}
          returnKeyType={'next'}
          autoCapitalize={'none'}
        />
        <CustomTextInput
          label={'RT / RW'}
          placeholder={'RT / RW'}
          returnKeyType={'next'}
          autoCapitalize={'none'}
        />
        <View style={styles.pickerContainer}>
          <Picker
            style={styles.picker}
            mode={'dropdown'}
            selectedValue={this.state.letterType}
            onValueChange={itemValue =>
              this.setState({ letterType: itemValue })
            }
          >
            <Picker.Item
              label={'Surat Keterangan Domisili'}
              value={'domisili'}
            />
            <Picker.Item label={'Pengantar PBB'} value={'pbb'} />
          </Picker>
        </View>
        <CustomButton
          label={'Kirim'}
          onPress={() => this.props.navigation.navigate('Confirmation')}
        />
      </ScrollView>
    )
  }

  renderLogin() {
    return (
      <View
        style={[
          styles.container,
          { alignItems: 'center', justifyContent: 'center' }
        ]}
      >
        <Text style={{ color: 'black' }}>
          Silahkan login untuk mengakses layanan
        </Text>
        <CustomButton
          label={'Login'}
          onPress={() => this.props.navigation.navigate('Login')}
        />
      </View>
    )
  }

  render() {
    if (this.state.loginState) {
      return this.renderContent()
    }
    return this.renderLogin()
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingVertical: 20,
    paddingHorizontal: 10
  },

  pickerContainer: {
    marginVertical: 5,
    borderWidth: 0.3,
    borderColor: constants.GREY,
    borderRadius: 5,
    width: constants.DEVICE_WIDTH * 0.9,
    backgroundColor: 'white'
  },

  picker: {
    width: constants.DEVICE_WIDTH * 0.9
  }
})
