/* @flow */

import React, { Component } from 'react'
import { View, Text, StyleSheet } from 'react-native'

import MapView from 'react-native-maps'

const now = new Date().getHours()

export default class Map extends Component {
  static navigationOptions = {
    title: now > 17 ? 'Kebersihan' : 'Motor Lorong'
  }

  render() {
    return (
      <MapView
        initialRegion={{
          latitude: -5.1698075,
          longitude: 119.4254829,
          latitudeDelta: 0.0922,
          longitudeDelta: 0.0421
        }}
        style={{ flex: 1, backgroundColor: 'white' }}
      />
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center'
  }
})
