/* @flow */

import React, { Component } from 'react'
import { View, Text, StyleSheet, FlatList, Alert } from 'react-native'

import EmployeeItem from '../../components/EmployeeItem'

export default class Home extends Component {
  constructor(props) {
    super(props)
    this.state = {
      list: LIST
    }
  }

  confirm(id) {
    Alert.alert('Konfirmasi', 'Konfirmasi permintaan ini?', [
      { text: 'Batal' },
      {
        text: 'Konfirmasi',
        onPress: () => {
          let list = this.state.list
          list.splice(id, 1)
          this.setState({ list: list })
        }
      }
    ])
  }

  render() {
    return (
      <View style={styles.container}>
        <FlatList
          contentContainerStyle={{ alignItems: 'center' }}
          data={this.state.list}
          renderItem={({ item, index }) => (
            <EmployeeItem
              position={'Permintaan Administrasi & SOP'}
              onPress={() => this.confirm(index)}
            />
          )}
        />
      </View>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1
  }
})

const LIST = [{ id: 0 }, { id: 1 }, { id: 2 }]
