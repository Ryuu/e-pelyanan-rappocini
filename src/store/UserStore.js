import { observable } from 'mobx'

class UserStore {
  @observable user = false
}

export default new UserStore()
