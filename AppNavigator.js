import React, { Component } from 'react'
import { Image, TouchableOpacity } from 'react-native'
import { StackNavigator, DrawerNavigator, TabNavigator } from 'react-navigation'

import constants from './src/config/constants'
import Drawer from './src/components/Drawer'

//Stack screens
import Confirmation from './src/screens/Confirmation'
import RegionDetail from './src/screens/RegionDetail'
import RegionProfile from './src/screens/RegionProfile'
import Login from './src/screens/auth/Login'
import EmployeeList from './src/screens/EmployeeList'
import HomeAdmin from './src/screens/admin/Home'

//Drawer Screens
import Home from './src/screens/Home'
import Organization from './src/screens/Organization'
import RegionList from './src/screens/RegionList'
import Contact from './src/screens/Contact'

//Service screens
import Form from './src/screens/service/Form'
import Map from './src/screens/service/Map'
import Employee from './src/screens/service/Employee'

const ICON_HAMBURGER = require('./assets/icon_hamburger.png')
const ICON_SERVICE = require('./assets/icon_service.png')

const TabNav = TabNavigator(
  {
    Form: { screen: Form },
    Map: { screen: Map },
    Employee: { screen: Employee }
  },
  {
    tabBarPosition: 'top',
    activeTintColor: 'white',
    tabBarOptions: {
      activeTintColor: 'white',
      inactiveTintColor: 'white',
      style: {
        backgroundColor: constants.PRIMARY_COLOR
      }
    },
    indicatorStyle: {
      borderBottomColor: 'white'
    }
  }
)

const DrawerNav = DrawerNavigator(
  {
    Home: { screen: Home },
    Organization: { screen: Organization },
    Service: {
      screen: TabNav,
      navigationOptions: {
        title: 'Layanan',
        drawerIcon: () => <Image source={ICON_SERVICE} />
      }
    },
    RegionList: { screen: RegionList },
    Contact: { screen: Contact }
  },
  {
    contentOptions: {
      activeTintColor: constants.PRIMARY_COLOR
    },
    contentComponent: props => <Drawer {...props} />
  }
)

const RootNav = StackNavigator(
  {
    Drawer: {
      screen: DrawerNav,
      navigationOptions: ({ navigation }) => ({
        title: 'E-Pelayanan Rappocini',
        headerLeft: (
          <TouchableOpacity
            style={{ flex: 1, paddingLeft: 25 }}
            onPress={() => navigation.navigate('DrawerToggle')}
          >
            <Image source={ICON_HAMBURGER} />
          </TouchableOpacity>
        )
      })
    },
    Confirmation: { screen: Confirmation },
    RegionDetail: { screen: RegionDetail },
    RegionProfile: { screen: RegionProfile },
    Login: { screen: Login },
    EmployeeList: { screen: EmployeeList },
    Home: {
      screen: HomeAdmin,
      navigationOptions: ({ navigation }) => ({
        title: 'Admin',
        headerLeft: (
          <TouchableOpacity style={{ flex: 1, paddingLeft: 25 }}>
            <Image source={ICON_HAMBURGER} />
          </TouchableOpacity>
        )
      })
    }
  },
  {
    navigationOptions: {
      headerTintColor: 'white',
      headerStyle: {
        backgroundColor: constants.PRIMARY_COLOR
      }
    }
  }
)

export default class App extends Component {
  render() {
    return <RootNav />
  }
}
